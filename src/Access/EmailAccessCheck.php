<?php

namespace Drupal\views_email_domain_access_plugin\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on user email domain.
 *
 * You can specify the '_email_domain' key on route requirements.
 * If you specify a single email domain,
 * users with that email domain with have access.
 * If you specify multiple ones you can conjunct them with by using a ",".
 */
class EmailAccessCheck implements AccessInterface {

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, AccountInterface $account) {

    // Requirements just allow strings, so this might be a comma separated list.
    $emailList = $route->getRequirement('_email_domain');

    $split = array_filter(array_map('trim', explode(',', strtolower($emailList))));

    $userEmail = trim(strtolower($account->getEmail()));
    if (!empty($userEmail)) {

      $emailDomain = explode('@', $userEmail);

      if (in_array($emailDomain[1], $split)) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::neutral();
  }

}
