<?php

namespace Drupal\views_email_domain_access_plugin;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * User email domains list.
 */
class UserEmail {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a user email domain list.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets all users email domains.
   *
   * @return array
   *   An array of email domains.
   */
  public function domain() {
    $options_array = [];
    $entity_storage = $this->entityTypeManager->getStorage('user');
    $entity_ids = $entity_storage->getQuery()
      ->condition('status', '1')
      ->execute();

    // Load all users.
    $users = $entity_storage->loadMultiple($entity_ids);

    // Extract users email i array.
    foreach ($users as $user) {
      if (!empty($user->get('mail')->value)) {
        $domain = explode('@', $user->get('mail')->value);
        // Replace dot to underscore as views base
        // configuration did not support dot value.
        $removeDot = strtolower(str_replace('.', '_', $domain[1]));
        $options_array[$removeDot] = strtolower($domain[1]);
      }
    }

    // Filter deulicate values.
    $unique = array_unique($options_array);
    return $unique;
  }

}
