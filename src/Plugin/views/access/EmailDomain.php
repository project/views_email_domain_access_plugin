<?php

namespace Drupal\views_email_domain_access_plugin\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Session\AccountInterface;
use Drupal\views_email_domain_access_plugin\UserEmail;

/**
 * Access plugin that provides email-domain-based access control.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "user_email_domain_access",
 *   title = @Translation("Email domain"),
 *   help = @Translation("Access will be granted on basis of email domain")
 * )
 */
class EmailDomain extends AccessPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * The user email storage.
   *
   * @var \Drupal\views_email_domain_access_plugin\UserEmail
   */
  protected $emailDomain;

  /**
   * Constructs a UserEmail object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\views_email_domain_access_plugin\UserEmail $emailDomain
   *   Array of Emails.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserEmail $emailDomain) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->emailDomain = $emailDomain;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('views_email_domain_access_plugin.views_email_domain_access')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {

    $domainList = implode(',', $this->options['domain']);
    $domainList = str_replace('_', '.', $domainList);
    $domainList = (string) $domainList;

    $split = array_filter(array_map('trim', explode(',', strtolower($domainList))));

    $userEmail = trim(strtolower($account->getEmail()));
    if (!empty($userEmail)) {

      $emailDomain = explode('@', $userEmail);

      if (in_array($emailDomain[1], $split)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {

    if ($this->options['domain']) {

      $domainList = implode(',', $this->options['domain']);
      $domainList = str_replace('_', '.', $domainList);

      $route->setRequirement('_email_domain', (string) $domainList);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {

    $count = count($this->options['domain']);

    if ($count < 1) {
      return $this->t('No domain(s) selected');
    }
    elseif ($count > 1) {
      return $this->t('Multiple domains');
    }
    else {
      $domainList = implode(',', $this->options['domain']);
      $domainList = str_replace('_', '.', $domainList);
      return $domainList;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['domain'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['domain'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Email Domains'),
      '#default_value' => $this->options['domain'],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $this->emailDomain->domain()),
      '#description' => $this->t('Only the checked domains will be able to access this display.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    $domain = $form_state->getValue(['access_options', 'domain']);
    $domain = array_filter($domain);

    if (!$domain) {
      $form_state->setError($form['domain'], $this->t('You must select at least one email domain if type is "by email domain"'));
    }

    $form_state->setValue(['access_options', 'domain'], $domain);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user.email_domain'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
