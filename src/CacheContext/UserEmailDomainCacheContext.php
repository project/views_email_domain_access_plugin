<?php

namespace Drupal\views_email_domain_access_plugin\CacheContext;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Defines the UserEmailDomainCacheContext service, for "per domain" caching.
 */
class UserEmailDomainCacheContext implements CacheContextInterface {
  /**
   * Proxy for the current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs an instance of the UserEmailDomainCacheContext class.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('User Email Domain');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $userEmail = trim(strtolower($this->currentUser->getEmail()));

    $emailDomain = '';

    if (!empty($userEmail)) {

      $emailDomain = explode('@', $userEmail);
      $emailDomain = $emailDomain[1];
    }
    return $emailDomain;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
