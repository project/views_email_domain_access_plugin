CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Description
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Views access plugin based on user email domain


DESCRIPTION
------------

The Views Email domain access plugin module provides you the option
to restrict access of the views page/block on the basis of user profile
email domains. After installation, you should see an "Email domain" option
with multi-select domain values inside "Access" option of the Views
"PAGE SETTINGS" or "BLOCK SETTINGS". 
This is similar to 'Role' option of Access option.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

No special configuration.

MAINTAINERS
-----------------------

Current Maintainers:
 * Dinkar Kumar (kumardinkar9) - https://www.drupal.org/u/kumardinkar9
